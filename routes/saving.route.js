var router = require('express').Router();
var fs = require('fs');
var path = require('path');
// var auth = require('../middle-ware/auth');
var savingController = require('../controller/saving.controller');

router.get('/', getSaving);
router.get('/balance/:id', getBalance);
router.post('/:id', createSaving);
// router.post('/:id', InBalance);
router.put('/:id', updateSaving);
router.delete('/:id', deleteSaving);


module.exports = router;

function getSaving(req, res, next) {
    savingController.getSaving(req.query)
        .then(function (savings) {
            res.send(savings);
        })
        .catch(function (err) {
            next(err);
        })
}

function getBalance(req, res, next) {
    var idSaving = req.params.id;
    var user = req.body;
    user._id = idSaving;
    console.log(user);
    savingController.getBalance(user)
        .then(function (user) {
            res.send(user)
        })
        .catch(function (err) {
            next(err);
        })
}

function createSaving(req, res, next) {
    var idSaving = req.params.id;
    var newSaving = req.body;
    // newSaving._id = idSaving;
    savingController.createSaving(idSaving,newSaving)
        .then(function (saving) {
            res.json(saving);
        })
        .catch(function (err) {
            next(err);
        })
}

function updateSaving(req, res, next) {
    var id = req.params.id;
    var saving = req.body;
    saving._id = id;
    savingController.updateSaving(saving)
        .then(function (saving) {
            res.send(saving);
        })
        .catch(function (err) {
            next(err);
        })
}

function deleteSaving(req, res, next) {
    var id = req.params.id;
    var saving = req.body;
    saving._id = id;
    console.log(id);
    savingController.deleteSaving(saving)
        .then(function (saving) {
            res.send(saving);
        })
        .catch(function (err) {
            next(err);
        })
}

//

// query = '{"likes": {$gt:10}, $or: [{"by": ""},{"title": ""}]}'
// db.mycol.find(query)

// 