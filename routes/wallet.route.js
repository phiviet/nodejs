var router = require('express').Router();
var fs = require('fs');
var path = require('path');
var walletController = require('../controller/wallet.controller');

router.get('/',getWallets);
router.post('/', createWallet);

module.exports = router;

function getWallets(req, res, next) {
    walletController.getWallets(req.query)
        .then(function (wallets) {
            res.send(wallets);
        })
        .catch(function (err) {
            next(err);
        })
}

function createWallet(req, res, next) {
    var newWallet = req.body;
    walletController.createWallet(newWallet)
        .then(function (wallet) {
            res.json(wallet);
        })
        .catch(function (err) {
            next(err);
        })
}