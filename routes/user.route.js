var router = require('express').Router();
var fs = require('fs');
var path = require('path');
var auth = require('../middle-ware/auth');
var userController = require('../controller/users.controller');

router.get('/', getUsers);
router.get('/saving/:id',getSavings);
router.post('/', createUser);
router.put('/:id',updateUser);
router.delete('/:id',deleteUser);

module.exports = router;

function getSavings(req, res, next){
    var idUser = req.params.id; 
    userController.getSavings(idUser)
        .then(function (user) {
            res.send(user);
        })
        .catch(function (err) {
            next(err);
        })
}

function deleteUser(req,res,next){
    var id = req.params.id;
    var user = req.body;
    user._id = id;
    console.log(id);
    userController.deleteUser(user)
    .then(function(user){
        res.send(user);
    })
    .catch(function(err){
        next(err);
    })
}

function updateUser(req,res,next){
    var id = req.params.id;
    var user = req.body;
    user._id = id;
    console.log(id);
    userController.updateUser(user)
    .then(function(user){
        res.send(user);
    })
    .catch(function(err){
        next(err);
    })
}

function getUsers(req, res, next) {
    console.log(req.query);
    userController.getUsers(req.query)
        .then(function (users) {
            res.send(users);
        })
        .catch(function (err) {
            next(err);
        })
}

function createUser(req, res, next) {
    var newUser = req.body;
    if (!newUser.email) {
        next({
            statusCode: 400,
            message: "Email is required"
        })
    } else if (!newUser.password) {
        next({
            statusCode: 400,
            message: "Password is required"
        })
    } if (!newUser.name) {
        next({
            statusCode: 400,
            message: "Name is required"
        })
    } else {
        userController.createUser(newUser)
            .then(function (user) {
                res.json(user);
            })
            .catch(function (err) {
                next(err);
            })
    }
}
//

// query = '{"likes": {$gt:10}, $or: [{"by": "laptrinhvien.io"},{"title": "MongoDB Overview"}]}'
// db.mycol.find(query)

// 