var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var savingSchema = new Schema({
    idwallet: {
        type: Schema.Types.ObjectId,
        ref: 'wallet'
    },
    iduser: {
        type:Schema.Types.ObjectId,
        ref:'user'
    },
    namesaving: {
        type:String,
        required: true
    },
    totalIn: {
        type:Number,
        default:0
    },
    totalOut: {
        type:Number,
        default:0
    },
    moneyend: {
        type:Number,
        required:true
    },
    transactionIn:[
        {
          name:String,
          money:Number
        }
    ],
    transactionOut:[
        {
          name:String,
          money:Number
        }
    ],
    enddate:{
        type:Date
    }
});

var saving = mongoose.model('saving', savingSchema);
module.exports = saving;