var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    savings:[{
        idwallet: {
            type: Schema.Types.ObjectId,
            ref: 'wallet'
        },
        namesaving: {
            type:String,
            required: true
        },
        totalIn: {
            type:Number,
            default:0
        },
        totalOut: {
            type:Number,
            default:0
        },
        moneyend: {
            type:Number,
            required:true
        },
        transactionIn:[
            {
              name:String,
              money:Number
            }
        ],
        transactionOut:[
            {
              name:String,
              money:Number
            }
        ],
        enddate:{
            type:Date
        }
    }],
    wallets: [{
        type: Schema.Types.ObjectId,
        ref: 'wallet'
    }]

});

userSchema.index({name:'text'});
// userSchema.index({name:'text',profession:'text'}),

var user = mongoose.model('user', userSchema);
module.exports = user;