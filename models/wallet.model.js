var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var walletSchema = new Schema({
    iduser: {
        type:Schema.Types.ObjectId,
        ref:'user'
    },
    namewallet: {
        type:String,
        required:true
    },
    money: {
        type:Number,
        required:true
    }
});

var wallet = mongoose.model('wallet', walletSchema);
module.exports = wallet;