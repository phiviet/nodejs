var Wallet = require('../models/wallet.model');
var User = require('../models/user.model');
var Savings = require('../models/saving.model');

module.exports = {
    getWallets: getWallets,
    createWallet: createWallet
}

function getWallets(query) {
   
    return Wallet.find()
        .then(function (users) {
            return Promise.resolve(users);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function createWallet(newWallet) {

    var wallet = new Wallet(newWallet);

    return wallet.save()
        .then(function (wallet) {
            return User.findById(wallet.iduser)
                .then(function (user) {
                    // console.log(wallet._id);
                    console.log(user.wallets);
                    user.wallets.push(wallet._id);
                    return user.save()
                        .then(function () {
                            return Promise.resolve(wallet);
                        })
                })
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}