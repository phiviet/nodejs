// var Saving = require('../models/saving.model');
var Wallet = require('../models/wallet.model');
var User = require('../models/user.model');
var mongoose = require('mongoose');
var Schema = mongoose.Schem

module.exports = {
    getSaving: getSaving,
    getBalance: getBalance,
    // InBalance: InBalance,
    createSaving: createSaving,
    updateSaving: updateSaving,
    deleteSaving: deleteSaving
}

function getSaving() {
    return Saving.find()
        .then(function (savings) {
            console.log(savings);
            return Promise.resolve(savings);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function getBalance(user) {
    return User.find({ _id: user._id })
        .then(function (user) {
            for (var i = 0; i < user[0].savings[0].transactionIn.length; i++) {
                user[0].savings[0].totalIn += user[0].savings[0].transactionIn[i].money;
            }

            for (var i = 0; i < user[0].savings[0].transactionOut.length; i++) {
                user[0].savings[0].totalOut += user[0].savings[0].transactionOut[i].money;
            }
            return Promise.resolve(user);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}


function createSaving(iduser, newsaving) {
    var isExisted = false;
    return User.findById(iduser)
        .then(function (user) {
            for (var i = 0; i < user.savings.length; i++) {
                if (user.savings[i].namesaving == newsaving.namesaving) {
                    isExisted = true;
                }
            }
            if (isExisted == true) {
                return Promise.reject({
                    statusCode: 400,
                    message: 'Name is existed'
                });
            }
            else {
                user.savings.push(newsaving);
                console.log(user.savings);
                return user.save()
                    .then(function () {
                        return Promise.resolve(user);
                    })                
            }

        })
}

function updateSaving(user) {
    return User.update({ _id: user._id }, user)
        .then(function (user) {
            return Promise.resolve(user);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}

function deleteSaving(saving) {
    return Saving.findOneAndRemove({ _id: saving._id })
        .then(function (saving) {
            console.log(saving);
            return Promise.resolve(saving);
        })
        .catch(function (err) {
            return Promise.reject(err);
        })
}